![LIDO logo](http://cidoc.mini.icom.museum/wp-content/uploads/sites/6/2018/12/LIDO_logo_main_240x153_bg-white_01.png "Title Text")

LIDO is an XML schema intended for delivering metadata, for use in a variety of online services, from an organization’s collections database to portals of aggregated resources, as well as exposing, sharing and connecting data on the web. Its strength lies in its ability to support the typical range of descriptive information about objects of material culture. It can be used for all kinds of object, e.g. art, cultural, technology and natural science and supports multilingual portal environments.

The LIDO schema is the result of a substantial redesign and enhancement of the CDWA Lite and museumdat schemas based on recommendations of the CDWA Lite/museumdat Working Group, community feedback and further CIDOC-CRM analysis. It mainly builds on CDWA and includes additional concepts to meet SPECTRUM requirements.

For more information on LIDO please refer to: [http://www.lido-schema.org](http://www.lido-schema.org)

The schema is developed at [https://gitlab.gwdg.de/lido/development](https://gitlab.gwdg.de/lido/development)

LIDO v1.1 is backwards compatible with LIDO v1.0. For a summary of changes please refer to the chapter "# What's new in LIDO v1.1". The full history of LIDO can be found in the [LIDO v1.0 specification, Chapter 2.3 History of the schema](http://www.lido-schema.org/schema/v1.0/lido-v1.0-specification.pdf).

Prepared for CIDOC LIDO Working Group by LIDO-DE Working Group.

Editors: Erin Coburn, Richard Light, Jutta Lindenthal, Gordon McKenna (Collections Trust), Regine Stein (Göttingen State and University Library), Axel Vitzthum (digiCULT Verbund e.G.), Michelle Weidling (Göttingen State and University Library)

Contributors: Detlev Balzer, Regine Heuchert (Technoseum), Angela Kailus (Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg), Herdis Kley, Marco Klindt (Zuse Institute Berlin), Markus Matoni (Göttingen State and University Library), Timo Schleier (Göttingen State and University Library), Francesca Schulze (German National Library), Martin Stricker (Hermann von Helmholtz-Zentrum für Kulturtechnik).

LIDO name and logo courtesy Rob Lancefield.

Copyright 2009-2020 ICOM-CIDOC for the LIDO Working Group.
License: [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)

# Get Started

This repository contains all relevant files of the LIDO standard. The schema is described as an XML Schema Definition, the documentation and a terminology. The most recent version is **LIDO v1.1** and is available for download here:

- [**LIDO v1.1 XML Schema Definition (XSD)**](http://www.lido-schema.org/schema/v1.1/lido-v1.1.xsd)
- [**LIDO v1.1 XML Schema Definition (QA extended XSD)**](http://www.lido-schema.org/schema/v1.1/lido-v1.1.xsd) (This version extends the schema with Schematron rules to improve quality assurance)
- [**Online documentation (HTML)**](http://www.lido-schema.org/schema/v1.1/lido-v1.1.html) (Explore the schema from the [root element](http://www.lido-schema.org/schema/v1.1/lido-v1.1.html#lido_lidoWrap))
- [LIDO Terminology](http://cidoc.mini.icom.museum/working-groups/lido/lido-technical/terminology/): [Documentation](http://cidoc.mini.icom.museum/working-groups/lido/lido-technical/terminology/lod-interface/), [Web interface](http://terminology.lido-schema.org/), [SPARQL](http://terminology.lido-schema.org/sparql)

**Previous versions of LIDO:**

- LIDO v1.0 is still available online. [XML Schema Definition](http://www.lido-schema.org/schema/v1.0/lido-v1.0.xsd) - [Documentation online](http://www.lido-schema.org/schema/v1.0/lido-v1.0-schema-listing.html) and for [download](http://www.lido-schema.org/schema/v1.0/lido-v1.0-specification.pdf)

# What's new in LIDO v1.1

For the development of LIDO v1.1 the following criteria for taking into account suggestions for changes and extensions have been defined:

- The suggestion requires modification of the schema, e.g. there is no way to express the information in the LIDO v1.0 schema.
- The suggestion is based upon a known use case from practical LIDO applications.
- The requirement is generic and in the scope of LIDO v1.0.
- The suggestion can be implemented in a backwards compatible way with LIDO v1.0.

## General changes

### Documentation

The schema docs are provided in a structured way as TEI elements as follows:

- the element description
- the element's label
- elements from other schemas to which the respective LIDO element is equivalent
- a note where a user can find more context about the element (this often refers to CDWA FULL, as LIDO elements are largely based on LIDO's predecessor CDWA Lite)
- recommended data values for controlled terms
- the following docs have been removed: 'How to record', 'Notes' (both outdated and largely merged into the element description)

Furthermore, the following additions have been made:

- machine-readable metadata is provided in a TEI header.
- each LIDO element/complexType/attribute is now referenceable by an ID. This ID is (in most cases) identical to its name and comes in handy for developing application profiles.

### Accompanying Documents

Apart from the documentation elements there are accompanying documents providing further information on LIDO elements:

- a **metadata crosswalk** provides a mapping of LIDOv1.1 elements to LIDOv1.0, CDWA Lite, museumdat, and Spectrum. CIDOC-CRM equivalents are provided in `metadata-crosswalk.xml`, but are not serialized because they are not complete yet.
- **links for further information** and **terminology recommendations**

### Schematron

We introduced Schematron as a second quality assurance mechanism. This is e.g. used to ensure that dates comply to the xs:dateTime requirements.

While there are Schematron rules in the schema itself which we consider as essential for data quality, more quality assuring rules can be found in the Quality Assurance LIDO Profile. This schema can be found in the [relevant GitLab repository](https://gitlab.gwdg.de/lido/profiles/-/tree/master/qa-profile?ref_type=heads).

## New Elements (and Why They Have Been Introduced)

- **applicationProfile**: Serves as an identifier for a LIDO application profile which has been developed by an institution or project.

- **collection**: A set of identifying and indexing information about the collection the object/work in focus is gathered into or is a member of.

- **conceptElementsSet**: Increases the schema's modularity.

- **displayRelatedWork**: A display element displayRelatedWork for the relatedWorkSet allows for transferring specific relationship information for presentation purposes while for the actual relationship type element (lido:relatedWorkRelType) terms from the LIDO Terminology should be used.

- **displayRepository**: A free-text description for designation of the institution of custody and, possibly, a descriptive indication of the exact location of the object while for repositoryName and repositoryLocation authorities should be used.

- **eventObjectMeasurements**: Indicates the dimensions or other measurements of the object/work as determined with respect to the described event, for instance a part addition or removal.

- **objectDescriptionRights**: Allows for setting separate rights information for the object description.

- **objectMaterialsTechSet/objectMaterialsTechWrap**: Allows for materials/technique information (meant like a physical characteristic of the object) outside of events.

- **sourceActorInRole**: Allows for providing source information about an actor in a certain role.

- **sourceRelatedWorkSet**: Allows for providing source information about a related work.

- **sourceRepositorySet**: Allows for providing source information about a current or former repository.

- **textAttributesSet**: Increases the schema's modularity.

- **vitalPlaceActor**: Allows for providing the birth/death/activity place of an actor.

## New Attributes (and Why They Have Been Introduced)

- **measurementsGroup**: Indicates the group of measurements given in multiple LIDO Measurement Set elements. Of relevance for the EODEM application profile.

- **mostNotableEvent**: Qualifies an eventSet as the most notable or significant event as designated by the describing institution.

## New Complex Types (and Why They Have Been Introduced)

- **conceptMixedComplexType**: A complexType allowing both free text and the elements defined in conceptElementsSet. This complexType is used for elements that only allowed free text in LIDO v1.0 but should be controllable with conceptID(s) and term(s) in LIDO v1.1.

- **rightsHolderComplexType**: Increases the schema's modularity. rightsHolder doesn't have to be defined fully twice.

## Changed Content Model: Mixed Content

The following elements can contain free text only in LIDO v.1.0, but can also provide controlled terms (e.g. from authoritative data) as an alternative via conceptComplexType in LIDO v1.1:

- attributionQualifierActor
- extentActor
- extentMaterialsTech
- extentMeasurements
- extentSubject
- formatMeasurements
- genderActor
- measurementType
- measurementUnit
- qualifierMeasurements
- scaleMeasurements
- shapeMeasurements

## Changed Descriptions

Definitions have generally been renamed to 'description'.
Almost all descriptions have been revised and specified for clarity.

## Other Changes

The following elements can  now hold additional attributes:

- genderActor allows for lido:type as attribute
- objectMeasurementsSetComplexType allows for lido:type as attribute
- rightsType allows for lido:type as attribute
- titleSet allows for lido:pref as attribute
- vitalDatesActor allows for lido:type as attribute

The following other changes have been made:

- <http://schemas.opengis.net/gml/3.1.1/base/gml.xsd> is now in use for GML validation
- instead of defining objectClassificationWrap, classificationWrap, and objectWorkTypeWrap twice, a reference has been introduced
